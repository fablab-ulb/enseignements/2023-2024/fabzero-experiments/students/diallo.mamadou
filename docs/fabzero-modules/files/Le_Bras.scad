$fn = 200; //face number
//Parametre du cube

longueur_cube = 40;
largeur_cube  = 5;
hauteur_cube  = 1;
translate([0, -2.5, 0])
cube([longueur_cube, largeur_cube, hauteur_cube]);

translate([0, 0, 0.5])
mirror([1, 0, 1])
cylinder(7, 3.9, 3.9);


//Paramettre pour les translations des sphres
x_translate = 40;
y_translate = 0;
z1_translate = 10.5;
z2_translate = 11.5;
rayon_sphere = 10;

difference(){
   translate([x_translate, y_translate, z1_translate])
   sphere(rayon_sphere);
   color("RED")
   translate([x_translate, y_translate, z2_translate])
   sphere(rayon_sphere);
}





