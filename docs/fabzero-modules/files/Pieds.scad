// File : Android.scad
// Author : Mamadou Diallo
// Date : 17 Octobre 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
//Global parameter
$fn = 200; //face number


//Parametre du premier cylindre et de sa translation
x_1_v               = 10;
y_1_v               = 12;
z_1_v               = 5;
longueur_cylindre_1 = 14;
rayon_cylindre_1    = 4;

//Parametre du deuxième cylindre et de sa translation
x_2_v               = 30;
y_2_v               = 12;
z_2_v               = 5;
longueur_cylindre_2 = 14;
rayon_cylindre_2    = 4;

//Parametre de la première translation
x1                  = 5;
y1                  = 0;
z1                  = 0;

//Parametre de la deuxième translation
x2                  = 25;
y2                  = 0;
z2                  = 0;
//Parametre des deux cubes
longueur_cube       = 10;
largeur_cube        = 24;
hauteur_cube        = 10;

//Les deux cubes
translate([x1, y1, z1])
cube([longueur_cube, largeur_cube, hauteur_cube]);

translate([x2, y2, z2])
cube([longueur_cube, largeur_cube, hauteur_cube]);

//Le cylindre gauche
translate([x_1_v, y_1_v, z_1_v])
cylinder(longueur_cylindre_1, rayon_cylindre_1, rayon_cylindre_1);
//Le cylindre droit
translate([x_2_v, y_2_v, z_2_v])
cylinder(longueur_cylindre_2, rayon_cylindre_2, rayon_cylindre_2);







    