# 4. Outil Fab Lab sélectionné

Durant cette période, nous devrons choisir entre deux formations distinctes, à savoir :

- Prototype éelctronique
- Découpe contrée par ordinateur et impression 3D pour réparation 

Personnellement, j'ai opté pour la deuxième formation qui était divisée en deux sessions réparties sur deux jours. Chaque session durait quatre heures.

## 4.1 Jour 1
## Partie 1 : Réparation des d'appareils electriques

Aujourd'hui, nous avons acquis des connaissances fondamentales sur la réparation des appareils électroniques. Il est intéressant de constater que lorsque nos dispositifs électroniques tombent en panne, il n'est pas toujours nécessaire de les jeter ou de les confier à des réparateurs. Parfois, la panne est simplement due à un petit composant comme un fusible grillé, ou à quelque chose d'autre que l'on peut facilement se procurer dans les magasins ou en ligne. En achetant ou en commandant ces pièces, nous avons la possibilité de les remplacer par nous-mêmes. Cela nous permet non seulement de réduire le gaspillage, mais aussi d'économiser de l'argent, car le recours à un réparateur ou le remplacement de l'appareil coûterait bien plus cher.

Pour ce faire, il suffit de disposer d'un petit outil permettant de diagnostiquer la panne, disponible à moindre coût sur internet. Lors de notre formation, nous avons utilisé un multimètre à cet effet.

![](images/images_doc_04/multi...jpg)


## - Détection de panne 

Pour détecter une panne dans un appareil électronique, voici quelques étapes générales que vous pouvez suivre :

- **Observation :** Commencez par une observation visuelle de l'appareil pour repérer tout signe visible de dommage ou de dysfonctionnement. Recherchez des traces de brûlure, des fils cassés, des composants abîmés ou toute autre anomalie visible.

- **Vérification de l'alimentation :** Assurez-vous que l'appareil est correctement branché et que l'alimentation électrique fonctionne. Vérifiez les fusibles, les prises et les interrupteurs pour vous assurer qu'ils fonctionnent correctement.

- **Diagnostic de base :** Utilisez des outils de mesure tels que des multimètres pour vérifier la continuité des circuits, la tension, la résistance, etc. Ceci peut vous aider à identifier les zones où se situe la panne.

- **Schémas électriques :** Si vous avez accès au schéma électrique de l'appareil (dans le manuel de l'utilisateur ou disponible en ligne), utilisez-le pour localiser les composants clés, les points de connexion et pour mieux comprendre le fonctionnement de l'appareil.

- **Test des composants :** Testez les composants électroniques tels que les résistances, les condensateurs, les diodes, les transistors, etc., à l'aide d'un multimètre ou d'autres outils de test. Vérifiez s'ils fonctionnent correctement.

- **Isolation des problèmes :** En utilisant la méthode de l'élimination progressive, isolez les parties fonctionnelles de celles qui ne fonctionnent pas. Cela peut vous aider à identifier plus précisément la zone où se situe le problème.

- **Réparation ou remplacement :** Une fois que vous avez identifié la composante ou la partie défectueuse, décidez si vous pouvez la réparer ou si elle doit être remplacée. Assurez-vous de suivre les procédures appropriées et utilisez des composants de remplacement compatibles.

- **Test final :** Une fois la réparation ou le remplacement effectué, testez à nouveau l'appareil pour vous assurer que la panne a été résolue et que l'appareil fonctionne correctement.

- **Conseils important:**

Il est important de noter que la réparation d'appareils électroniques peut être complexe et peut nécessiter des connaissances spécialisées. Si vous n'êtes pas sûr de vos compétences ou si l'appareil est couvert par une garantie, il peut être préférable de faire appel à un professionnel qualifié pour la réparation. De plus, assurez-vous de toujours travailler en toute sécurité, en particulier lorsqu'il s'agit de manipuler des appareils électroniques sous tension.

Durant le cours, on nous a remis un petit boîtier contenant un circuit simple, que nous devions réparer.

![](images/images_doc_04/Boite..jpg)


J'ai entamé en dévissant d'abord le boîtier afin d'accéder au circuit qu'il renfermait.

![](images/images_doc_04/boite2.png)

Ensuite, j'ai suivi les étapes énumérées précédemment. Après avoir examiné attentivement le circuit sans détecter d'anomalie, j'ai procédé à l'étape suivante qui concernait l'alimentation. Lorsque j'ai mesuré la tension de la pile, j'ai réalisé qu'elle était déchargée et j'ai demandé à obtenir une pile de remplacement. Après avoir changé la pile, la lampe s'est allumée. La panne était donc due à un problème de tension provenant de la pile.



## Partie 2 : Impression 3D pour réparation 
Dans cette seconde partie, nous étions invités à apporter un objet de notre foyer qui était cassé ou endommagé et que nous souhaitions réparer. J'ai choisi d'apporter mon répéteur WiFi qui a malheureusement eu l'un de ses bras cassé par mon neveu.


De base, elle ressemble à ça.

![](images/images_doc_04/rep1..jpg)


Vous pouvez voir ici la pièce manquante


![](images/images_doc_04/pieces..jpg)

Pour y parvenir, j'ai dû démonter le répéteur afin d'obtenir la pièce non endommagée, dans le but de pouvoir l'imprimer en reproduisant exactement la même. J'ai eu de la chance car les deux pièces étaient identiques.

![](images/images_doc_04/demont.jpg)

Voici la pièce endommagée que j'aimerais réparer.

![](images/images_doc_04/imprim..jpg)


### a. Première tentative de réparation 

Après avoir élaboré et produit les deux pièces distinctes, positionnées respectivement à gauche et à droite dans l'image, j'ai procédé à leur assemblage. Cependant, une observation minutieuse révèle des divergences significatives par rapport à la pièce centrale. Ces variations, bien que subtiles, rendent les pièces latérales incompatibles avec l'utilisation prévue. Malgré mes efforts de conception et d'impression, il semble nécessaire d'ajuster certains paramètres ou de revoir le processus pour obtenir une cohérence entre les différentes pièces.

![](images/images_doc_04/tentative1.jpg)

### b. Deuxième tentative de  réparation 

Dans cette seconde tentative, une nouvelle idée a émergé. J'ai décidé d'utiliser FreeCAD pour redessiner l'image, afin d'obtenir la forme et les dimensions correctes. Ensuite, je convertirai ce dessin en un modèle 3D avant de le produire avec l'imprimante. Malheureusement, je rencontre actuellement quelques difficultés dans la manipulation du logiciel FreeCAD. Pour résoudre ces problèmes, je consulte des tutoriels sur YouTube afin de maîtriser son utilisation. Je suis convaincu que cette approche de conception aboutira.

![](images/images_doc_04/tentative2.png)

 Restez à l'écoute pour la suite de l'aventure.

 ### c. Résulat final 

 C'est avec enthousiasme que je partage une excellente nouvelle : après avoir visionné plusieurs tutoriels sur YouTube concernant l'utilisation de FreeCAD, j'ai réussi à concrétiser mon idée. J'ai pu créer la pièce en 3D avec succès. Ce succès représente une avancée significative dans le processus de conception et d'impression. La persévérance dans l'apprentissage de l'outil a porté ses fruits, et je suis maintenant prêt à passer à l'étape suivante de l'impression. Je suis ravi des progrès réalisés et confiant dans la qualité du résultat final.

 Voici un apperçu de la pièce sur Freecad et PrusaSlicer.

 ![](images/images_doc_04/3D_freecad.png)

 ![](images/images_doc_04/3D_prus.png)


 Après plusieurs tentatives d'impression, j'ai finalement obtenu une taille qui correspond presque à celle de la pièce d'origine. Elle s'insère parfaitement à l'endroit prévu et remplit également correctement sa fonction.


![](images/images_doc_04/pices.jpg)


![](images/images_doc_04/fin.jpg)


## 4.2 - Jour 2

### Découpe contrée par ordinateur

Pendant cette deuxième journée, l'objectif était d'acquérir des compétences pour concevoir et préparer des fichiers CAO 2D afin de les découper au laser. Avant le début des cours, les étapes préliminaires étaient les suivantes :

- Installation d'un logiciel d'imagerie vectorielle 2D tel que Inkscape et suivi de quelques tutoriels de base sur Inkscape. Vous pouvez également télécharger et installer l'application Inkscape en cliquant [ici](https://inkscape.org/), ainsi que consulter les tutoriels de base en cliquant [ici](https://inkscape.org/learn/tutorials/).
- Lecture du manuel **Réparer avec impression 3D** que nous avons reçu pendant le cours et étude du  [ce manuel de découp laser](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut), car l'utilisation du laser peut présenter des risques si nous ne disposons pas des informations essentielles. Ce manuel renferme toutes les données nécessaires concernant l'utilisation sécuritaire du laser.


Pour nous aider à déterminer le réglage optimal pour notre matériau, le fabLab met à notre disposition des grilles de test comprenant différentes puissances et vitesses. Voici une photo de cet outil.

![](images/images_doc_04/grilletest1..jpg) ![](images/images_doc_04/grilletest2..jpg)

## 4.2.1 Lasersaure
Cette est surtout utilisée pour la découpe vectorielle et elle est la machine la plus puissante des trois. Sa puissance de laser est de 100 Watts et elle réponds aux besoins des fabricants, des designers, des architectes. Elle est moins automatique par rapport aux autres pour l'utiliser, il faut faire certains ajustements manuellement. Vous pouvez cliquer [ici](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Lasersaur.md?ref_type=heads) pour plus de detaills.

## 4.2.2 Modélisation et utilisation

Pour faire un essayage, j'ai modelisé une image vectorielle sur Inkscape.

![](images/images_doc_04/RecINKscape1.jpg)

Une fois la modélisation finit, il faut enregistrer le fichier au format svg ensuite mettre ce fichier sur une clé USB affin de le placer dans l'ordinateur connecté au laser.

![](images/images_doc_04/LaserSaur3.jpg)

##4.2.3 Le kerf
Nous avons utilisé le **Kerf** pour évaluer la différence entre le dessin sur l'ordinateur et l'objet découpé, constatant ainsi un trait de scie de 0,15 mm.

![](images/images_doc_04/OutilKerf2..jpg)

## 4.2.4 Epilog Fusion Pro 32

L'Epilog Fusion Pro 32 est une machine de gravure et de découpe laser. Dotée d'une puissance laser moins performante par rapport à la précédente avec un maximal 60W et qui est également ajustable. Elle peut travailler sur divers matériaux tels que le bois, le plastique, le verre et certains métaux. Avec une interface conviviale, cette machine nous permet de contrôler le processus via un logiciel dédié, assurant une grande précision pour une variété d'applications allant de la personnalisation de produits à la fabrication de prototypes. Les spécifications exactes peuvent varier selon nos configurations personnalisées et nos besoins spécifiques.

### Voici quelques caractéristiques
- Surface de découpe : 81 x 50 cm
- Hauteur maximale : 31 cm
- Puissance du LASER : 60 W
- Type de LASER : CO2 (infrarouge)

Pour avoir plus de détails, vous pouvez tout simplement cliquez [ici](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Epilog.md?ref_type=heads#ouvrir-le-fichier-dans-inkscape)


## 4.2.5 Muse à spectre complet

Nous n'avons  malheureusement pas eu le temps de tester cette machine, mais par curiosité, je suis allé voir des informations la concernant sur internet et aussi sur les documentations du FabLab. J'ai puis voir que cette dernière est la plus faible en matière de puissance car sa puissance maximale est de 40W et elle a aussi un type en plus par rapport aux deux autres qu'on a puis voir et ce type est **pointeur Rouge**.

### Voici quelques caractéristiques

- Surface de découpe : 50 x 30 cm
- Hauteur maximale : 6 cm
- Puissance du LASER : 40 W
- Type de LASER : CO2 (infrarouge) + pointeur rouge
- Logiciel : Retina Graver


Vous pouvez cliquez [ici](PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6) pour suivre des tutos sur youtube en anglais concernant son utilisation et [ici](https://fslaser.com/material-test/) pour voir les réglages.

# 4.2.6 Conception d'une pièce et l'utilisation de l'Epilog Fusion Pro 32

### Travail personnel 

Je cherchais un design me permettant de tester les paramètres de découpe pour réussir l'emboîtement des pièces. C'est ainsi que j'ai pris la décision de concevoir un Dé sous forme d'une boite. Voici la conception sur Inkscape.

![](images/images_doc_04/laser.png)

J'ai également capturé quelques photos pendant la découpe.

![](images/images_doc_04/decoupe_laser.jpg)


Le résultat final.

![](images/images_doc_04/box_assemblée.jpg)


































