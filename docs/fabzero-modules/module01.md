# 1. Gestion de projet et documentation

L'objectif principal de cette unité est de permettre aux participants d'acquérir une maîtrise des outils simples mais efficaces pour la documentation et le partage de celle-ci à l'aide de systèmes de contrôle de version. En outre, cette unité vise à fournir des conseils essentiels pour réussir la gestion de projets.

### Outils essentiels

- Gittlab 
- GitBash [install](https://git-scm.com/downloads)
- Visual studio code [install](https://code.visualstudio.com/download)
- Windows powershell [install](https://learn.microsoft.com/fr-fr/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.4)


## 1.1 Gitlab

Personnellement, j'ai déjà eu l'occasion de travailler avec Git par le passé, donc j'avais déjà installé Git sur mon ordinateur. En ce qui concerne l'éditeur de fichiers, j'utilise Visual Studio, un environnement de développement intégré (IDE) conçu par Microsoft. Il est largement utilisé pour le développement de logiciels, d'applications web, mobiles et divers autres projets informatiques.

Pour télécharger et installer Git, c'est simple. Il vous suffit de cliquer [ici](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git) pour obtenir toutes les informations nécessaires à son installation sur votre ordinateur.


## 1.2 Création du site personnel

Pour débuter la création de mon site personnel, j'ai tout d'abord créé un compte sur GitLab. Une fois cette étape accomplie, j'ai cherché mon dossier personnel dans le projet du cours (FabLab Ulb) étant donné que je suis inscrit à celui-ci. Le chemin d'accès est le suivant : fablab-ulb/enseignements/2023-2024/fabzero-experiments/students.


## 1.3 Protocole SSH

Une fois Git installé et le compte GitLab créé, il est maintenant nécessaire d'établir une connexion sécurisée entre mon ordinateur et le serveur GitLab. Cette connexion est établie via le protocole SSH, en créant une paire de clés SSH. Cette paire se compose d'une clé publique qui sera ajoutée à notre compte GitLab et d'une clé privée que nous devrons conserver pour nous-même.

Avant d'entamer la procédure de génération de cette paire de clés, il est important de choisir le type de clé à utiliser.

Pour générer la clé SSH, vous pouvez cliquer  [ici](https://docs.github.com/fr/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) et suivre les instructions indiquées.

<!-- ... 
Une fois Git installé et le compte gitLab crée, il faut à présent créer une connexion sécurisée entre mon ordinateur et le serveur de Gitlab. Le protocole SSH est ce qui va nous permettre d’établir cette connexion par la création d’une paire de clé SSH. Dans cette paire l’une des clé est publique et sera par la suite ajoutée à notre compte Gitlab, l’autre est personnelle et elle sera à garder pour nous.
Avant de commencer la procédure pour générer cette paire de clé, il faut d’abord choisir quel type de clé utilisé. 
Pour la création de la clé SSH, Cliquez [ici](https://docs.github.com/fr/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) et suivez les procédures.-->


## 1.4 Cloner le projet

Le processus de clonage implique la création d'une copie de votre projet sur votre ordinateur, vous permettant ainsi de travailler directement depuis celui-ci.

Pour commencer, rendez-vous sur votre page GitLab et copiez votre chemin d'accès. Vous trouverez ci-joint une image illustrant la procédure à suivre.


<!-- ... Le clonage consiste à créer une copie de votre projet sur votre ordinateur pour pouvoir travailler directement à partir de celui-ci.
Tout d'abord, allez sur votre page gitLab et copiez votre chemin. Voici-ci joint une photo qui montre comment le faire.-->

![](images/images_doc_01/clone.jpg)

Une fois cette étape réalisée, ouvrez votre terminal ou l'invite de commande et choisissez le répertoire dans lequel vous souhaitez cloner le projet. Utilisez la commande 'cd' pour vous déplacer vers ce répertoire spécifique. Ensuite, utilisez la commande 'git clone' suivie de l'URL du référentiel GitLab. Par exemple, si l'URL est **https://gitlab.com/nom-utilisateur/nom-du-projet.git**, vous exécuterez la commande suivante :
**git clone https://gitlab.com/nom-utilisateur/nom-du-projet.git**.

Pour vérifier que l'opération s'est déroulée correctement, utilisez la commande 'ls' à la fin. Vous pourrez ainsi visualiser le nouveau dossier créé.

<!--Une fois cela fait, ouvrez votre terminal ou invite de commande et choisissez le répertoire où vous souhaitez cloner le projet. Vous pouvez utiliser la commande "cd" pour vous déplacer vers le répertoire souhaité. Utilisez la commande git clone suivie de l'URL du référentiel GitLab. Par exemple, si l'URL est **https://gitlab.com/nom-utilisateur/nom-du-projet.git**, vous exécuterez la commande;  **git clone https://gitlab.com/nom-utilisateur/nom-du-projet.git**Pour vérifier que cela à fonctionner, vous pouvez utiliser la commande ls à la fin et vous verrez le nouveau dossier qui est crée.-->


## 1.5 Modification et mise à jour de la page

Comme j'utilise Visual Studio pour modifier mes fichiers, j'ai simplement installé l'extension Git dans Visual Studio, configuré et cloné la référence. Voici les étapes :

<!--Comme j'utilise Visual Studio pour la modification de mes fichier, J'ai juste installer l'extension de git dans le Visual Studio, configurer et cloner la référence. Voici les étapes: -->

### 1.5.1 Installer l'extension Git dans Visual Studio :

- Une fois que Visual Studio est installé, ouvrez Visual Studio.
- Allez dans le menu "Extensions" -> "Gérer les extensions".
- Recherchez "Git" dans la barre de recherche des extensions.
-Installez l'extension "GitHub Extension for Visual Studio" ou une autre extension Git de votre choix.

### 1.5.2 Configurer Git dans Visual Studio :

- Après avoir installé l'extension Git, redémarrez Visual Studio si nécessaire.
- Allez dans "Outils" -> "Options".
- Sous "Source Control", assurez-vous que "Git" est sélectionné comme système de contrôle source par défaut.

### 1.5.3 Cloner un référentiel Git dans Visual Studio :

- Une fois Git configuré, vous pouvez cloner un référentiel Git en utilisant les fonctionnalités intégrées de Visual Studio.
- Allez dans "Fichier" -> "Cloner un dépôt".
- Entrez l'URL du référentiel Git que vous souhaitez cloner, puis suivez les instructions pour terminer le processus de clonage.

Une fois cela fait, vous pourrez utiliser les fonctionnalités Git directement depuis l'IDE pour gérer vos référentiels, effectuer des commits, des poussées (push), des tirages (pull) et bien d'autres opérations de gestion de code source.


![](images/images_doc_01/push1..jpg)



![](images/images_doc_01/push2..png)


![](images/images_doc_01/push3..png)


Suite à ces trois étapes, tout est désormais en ligne _-_.

# 2. Compression d'images, vidéos et importation sur notre site

Pour assurer un site web performant, plusieurs aspects doivent être pris en considération, dont la gestion des temps de chargement, la maîtrise de l'espace de stockage et l'adaptation aux différents appareils mobiles. Une approche efficace pour résoudre ces problèmes réside dans la compression des vidéos et des images.

Il existe diverses méthodes pour compresser une image. Vous pouvez opter pour l'utilisation d'outils en ligne ou télécharger des logiciels spécialisés dans ce domaine. Pour ma part, j'ai choisi [Gimp](https://www.gimp.org/downloads/). C'est un logiciel facile à prendre en main et doté d'une large gamme d'outils. J'ai suivi un tutoriel sur [YouTube](https://www.youtube.com/watch?v=vF3Jx4qkINc) pour me former à son utilisation.

Voici un aperçu de la manière d'importer un fichier dans une application et les résultats obtenus.

![](images/images_doc_01/gimp.png)


Image origiale sans compression avec taille : 493 ko

![](images/images_doc_01/bx.JPG)

Image après compression avec taille : 143 ko

![](images/images_doc_01/bx1.JPG)


**Attention** Si vous diminuez considérablement la taille de votre image, vous risquez de compromettre sa qualité. Il est essentiel de trouver un juste équilibre afin de ne pas réduire excessivement sa taille au détriment de sa qualité.

Pour intégrer une image dans votre site, il vous suffit de placer l'image dans le même dossier que votre fichier .md, puis d'écrire : **![](images/nom de l'image)**.


Pour compresser une vidéo, vous pouvez simplement utiliser un site dédié à cet effet, comme [celui-ci](https://www.freeconvert.com/) par exemple.


# 3. Gestionn de projet

Afin de nous aider à relativiser devant l'ampleur considérable du travail qui nous est confié, notre professeur a partagé avec nous diverses techniques approfondies de gestion de projet. L'assimilation de ces approches s'avère essentielle pour mieux appréhender le projet de groupe qui nous attend dans quelques semaines.

Parmi les différentes méthodes présentées, certaines ont particulièrement retenu mon attention et se démarquent par leur efficacité :

- **La décomposition en petites tâches :** L'idée de se fixer des objectifs au quotidien revêt une importance cruciale. Cette approche nous encourage à éviter la procrastination en nous incitant à travailler de manière constante. En décomposant le projet en tâches journalières, on prévient le risque d'accumuler un travail colossal à la dernière minute, ce qui contribue significativement à réduire le stress.

- **Hiérarchisation des tâches par ordre d'importance :** Une autre stratégie pertinente consiste à aborder en premier lieu les tâches jugées les plus volumineuses et complexes. En s'attaquant aux aspects les plus exigeants du projet dès le départ, on facilite non seulement la progression, mais on optimise également l'utilisation du temps.

- **Gestion du temps :** Une compréhension précise du délai imparti pour chaque aspect du travail demeure primordiale. Une fois que nous sommes conscients du temps alloué, il devient possible de définir des délais spécifiques pour chaque petite tâche, préalablement décomposée. À l'échéance de chaque délai, il est impératif de passer à la tâche suivante, même si la précédente n'est pas entièrement achevée. Cette approche stratégique s'inscrit dans la philosophie de la spirale, suggérant de finaliser rapidement un projet, même de manière minimale, pour ensuite revenir dessus et le perfectionner progressivement.
