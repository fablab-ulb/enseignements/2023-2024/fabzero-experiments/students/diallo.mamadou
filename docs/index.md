## Acceuil

Bonjour et bienvenue sur ma page Fablab pour le cours [2023-2024 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/class-website/).

## About me

![](images/photo..jpg)


Bonjour ! Je suis Mamadou Oury DIALLO, étudiant en deuxième année de licence en informatique à l'Université Libre de Bruxelles. Actuellement, je suis à Bruxelles pour poursuivre mes études.

N'hésitez pas à visiter ce site pour découvrir mon travail !

## My background

Je suis originaire d'une charmante ville nommée Kindia, située en République de Guinée. J'ai grandi là-bas et suivi une partie de ma scolarité jusqu'à mes 17 ans, avant de déménager avec ma famille pour nous installer au Grand-Duché du Luxembourg. J'y ai terminé mes études secondaires et après l'obtention de mon diplôme, je me suis directement inscrit à l'Université Libre de Bruxelles il y a deux ans. Depuis lors, j'ai passé la majeure partie de ma vie ici, rentrant seulement de temps en temps pendant les weekends pour retrouver ma famille.

## Précédent travail

Mon travail précédent s'est déroulé au Luxembourg, au sein d'une entreprise remarquable spécialisée dans les terminaux de paiement. J'étais très satisfait car le travail que j'effectuais était en adéquation avec mes études, ce qui m'a permis d'entrevoir ce qui m'attendrait après la fin de mes études.

