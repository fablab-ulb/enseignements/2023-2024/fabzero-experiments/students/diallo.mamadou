// File : Android.scad
// Author : Mamadou Diallo
// Date : 17 Octobre 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

module creer_cube(w, d, h){
    cube([w, d, h]);
}
module creer_cylinder(h, r1, r2){
    cylinder(h, r1, r2);
}
//Global parameter
$fn = 200; //face number

//__________________________________________________________________________________________________________________________________________
                                                                //VENTRE
//Le cube
longueur_v = 40;
largeur_v = 24;
hauteur_v = 40;

//La transation_premier trou et cylindre
x_1_v = 10;
y_1_v = 12;
z_1_v = -6;
longueur_c_1 = 14;
rayon_c_1 = 4;

//La translation_deuxième trou et cylindre
x_2_v = 30;
y_2_v = 12;
z_2_v = -6;
longueur_c_2 = 14;
rayon_c_2 = 4;

//Code du ventre
difference(){
    //Le cube du ventre
    creer_cube(longueur_v, largeur_v, hauteur_v);
    //Le trou gauche
    translate([x_1_v, y_1_v, z_1_v])
    cylinder(longueur_c_1, rayon_c_1, rayon_c_1);
    //Le trou droit
    translate([x_2_v, y_2_v, z_2_v])
    cylinder(longueur_c_2, rayon_c_2, rayon_c_2);
    //Les trous pour les mains
    mirror([1, 0,1]){
    color("REd")
    translate([-30, 12, -5])
    cylinder(10, 4, 4);
    color("RED")
    translate([-30, 12, -42])
    cylinder(10, 4, 4);
    }
    
}

//___________________________________________________________________________________________________________________________________________________
                                                               //Coup
//Parametre de la translation et du coup
x_t_coup = 4;  
y_t_coup = 4;
z_t_coup = 40;
longueur_coup = 32;
largeur_coup = 16;
hauteur_coup = 4;

//Code du coup
color("White")
translate([x_t_coup, y_t_coup, z_t_coup])
creer_cube(longueur_coup, largeur_coup, hauteur_coup);

//___________________________________________________________________________________________________________________________________________________
                                                               //Tête
//Pour la tête, nous allons tout dabord mettre un cube et en suite mettre les yeux. 
//Parametre du cube, les translation et les cylindre
//Première translation, cube, yeux.
                    //Translation
x_1_t_t = 0;
y_1_t_t = 0;
z_1_t_t = 44;
                    //Le cube
longueur_cube_t = 40;
largeur_cube_t = 24;
hauteur_cube_t = 16;
                    //Les yeux
//Côté gauche                 
x_t_o_g = 10;
y_t_o_g = -60;
z_t_o_g = -30;

hauteur_cy_o_g_d = 40;
rayon_cy_o_g_d = 2;

//Côté droit
x_t_o_d = 30;
y_t_o_d = -60;
z_t_o_d = -30;

//Code du cupe de la tête et les yeux.
difference(){
    translate([x_1_t_t, y_1_t_t, z_1_t_t])
    creer_cube(longueur_cube_t, largeur_cube_t, hauteur_cube_t);
                        //Les Yeux
    //Gauche
    mirror([0, 1, 1]){                            //mirror pour changer l'orientation des cylindres
    translate([x_t_o_g, y_t_o_g, z_t_o_g])
    creer_cylinder(hauteur_cy_o_g_d, rayon_cy_o_g_d, rayon_cy_o_g_d);
        
    //Droit
    translate([x_t_o_d, y_t_o_d, z_t_o_d])
    creer_cylinder(hauteur_cy_o_g_d, rayon_cy_o_g_d, rayon_cy_o_g_d);
    }
}

//_____________________________________________________________________________________________________________________________________________
//Cette deuxième partie consiste l'arc de cercle de la tête
//Parametre, pour les translation, nous aurons x_t1, x_t2,........ et pour les cubes, l1 = longueur cube1, l2 = longueur cube2.........
//pour les largeurs, L1 = largeur cube1, L2 = largeur cube2...........
x_t0 = 20;
y_t0 = 12;
z_t0 = 50;

//t1
x_t1 = -12;
y_t1 = -16;
z_t1 = 0;
//c1
l1  = 64;
L1  = 60;
h1  = 60;
//t2
x_t2 = -12;
y_t2 = -4;
z_t2 = 56;
//c2
l2  = 12;
L2  = 32;
h2  = 20;
//t3
x_t3 = 40;
y_t3 = -4;
z_t3 = 56;
//c3
l3  = 12;
L3  = 32;
h3  = 20;
//t4
x_t4 = -4;
y_t4 = 24;
z_t4 = 56;
//c4
l4  = 48;
L4  = 20;
h4  = 20;
//t5
x_t5 = -4;
y_t5 = 20;
z_t5 = 56;
//c5
l5  = 48;
L5  = -20;
h5  = 40;

difference(){
    translate([x_t0,y_t0, z_t0])           //t0
    sphere(26, $fn=100);
        translate([x_t1, y_t1, z_t1])      //t1
        creer_cube(l1, L1, h1);            //c1                 
        //Côté Gauch
        translate([x_t2, y_t2, z_t2])      //t2
        creer_cube(l2, L2, h2);            //c2
        //Côté droit
        translate([x_t3, y_t3, z_t3])      //t3
        creer_cube(l3, L3, h3);            //c3
        //Derière
        translate([x_t4, y_t4, z_t4])      //t4
        cube([l4, L4, h4]);                //c4
        //Devant
        translate([x_t5, y_t5, z_t5])       //t5
        cube([l5, L5, h5]);                 //c5
        
        translate([x_t5, -y_t5, z_t5])
        creer_cube(48, 20, 20);
    
                        //Les Yeux
    //Gauche
    mirror([0, 1, 1]){                            //mirror pour changer l'orientation des cylindres
    translate([x_t_o_g, y_t_o_g, z_t_o_g])
    creer_cylinder(hauteur_cy_o_g_d, rayon_cy_o_g_d, rayon_cy_o_g_d);
        
    //Droit
    translate([x_t_o_d, y_t_o_d, z_t_o_d])
    creer_cylinder(hauteur_cy_o_g_d, rayon_cy_o_g_d, rayon_cy_o_g_d);
    }
    
}


    
    
    

//_________________________________________________________________________________________________________________________________________________
                                                                //Les Oreilles
//Code de la translation côté droit
x_t_or_d = 10;
y_t_or_d = -75;  
z_t_or_d = -24;
//Code de la translation côté gauche
x_t_or_g = 30;
y_t_or_g = -75;
z_t_or_g = -24;
//Cylindre droit et gauche
l_cy_or_d = 24;
L_cy_or_d = 2;
h_cy_or_d = 2;


mirror([0, 1, 1]){
    //Droit
    translate([x_t_or_d, y_t_or_d, z_t_or_d])       //t_or_d
    cylinder(l_cy_or_d, L_cy_or_d, h_cy_or_d);      //cy_or_d
    //Gauche
    translate([x_t_or_g, y_t_or_g, z_t_or_g])   
    creer_cylinder(l_cy_or_d, L_cy_or_d, h_cy_or_d);       //c_or_g
}








    


 








    