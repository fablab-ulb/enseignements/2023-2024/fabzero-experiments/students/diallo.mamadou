# 3. Impression 3D

Bonjour, dans cette unité, l'objectif est la suivante :  

  - Identifiez les avantages et les limites de l’impression 3D.
  - Appliquer des méthodes de conception et des processus de production à l’aide de l’impression 3D.

Dans le processus, j'ai initialement téléchargé et installé le logiciel PrusaSlicer sur mon ordinateur. Ensuite, j'ai suivi le tutoriel d'impression 3D fourni par le professeur afin de me familiariser avec le logiciel. Une fois cela accompli, j'ai procédé à la préparation du G-code à l'aide du logiciel slicing, en vue d'imprimer la pièce 3D que j'avais conçue la semaine précédente. En guise de dernière étape, j'ai ajusté mon design pour que le temps d'impression de ma pièce ne dépasse pas les 20 à 40 minutes.

Pour télécharger le logiciel, cliquez tout simplement [ici](https://help.prusa3d.com/article/install-prusaslicer_1903).

## 3.1 Préparation du code G

### a - Export et import le fichier

La première étape consiste à exporter le fichier que nous avons créé sur OpenSCAD au format STL, afin que le PrusaSlicer puisse le prendre en charge. Voici la marche à suivre :

  - Calculer le rendu
  - Exporter sous format STL

![](images/images_doc_03/Export.png)


### b - Interface PrusaSlicer

Voici l'interface d'accueillie du logiciel PrusaSlicer et comment importé le fichier STL qu'on a généré dans logiciel OpenSCAD.

![](images/images_doc_03/PresuSlice1.png)
### c - Réglages des paramètres 

Une fois le document importé,  nous aurons besoin de choisir la face et d'ajuster les réglages d'impression pour avoir une bonne impression.
Voici les étapes du réglage à faire pour cela : 

![](images/images_doc_03/Prusa2.png)


## 3.2 Sélectionnez l'imprimante

![](images/images_doc_03/printer%20selection.png)

Si le nom de l'imprimente n'appareille pas,vous pouvez simplement suivre ces différentes étapes :

  - Cliquez sur **Ajouter/Supprimer des imprimantes** (Moi j'ai dû télécharger les documents car ils étaient pas encore téléchargés)
  - Rechercher le modèle correspondant
  - Cochez la case Buse **0,4 mm buse**
  - Cliquez sur le bouton **Fin**

### a - Sélection des filaments

**Attention** ! Les filaments à base d'**ABS**, d'**ASA** ou de **PS** dégagent des gaz nocifs lors de l'impression. Il est donc interdit de les utiliser pendant les heures d'ouverture du fablab.

Dans la liste **Filament**, vous pouvez sélectionner un pré-réglage de filament dans la base de données du slicer. En cliquant sur l'option **Ajouter/Enlever des filaments**, vous avez accès au catalogue de filaments qui ont déja un pré-réglage.

**Filament :**
![](images/images_doc_03/PLA.png)

Si votre filament n'est pas dans la base de données, vous pouvez essayer un réglage générique. Par exemple, le réglage **Generic PLA** fonctionnera avec tous les filaments à base de PLA, mais pas toujours de façon optimale. L'idéal est de vérifier les réglages en les comparateurs avec les informations indiquées sur la bobine de filament que vous allez utiliser. Dans l'onglet **Paramètres du filament**, vous pouvez modifier les températures de l'extrudeur et du plateau.

### b - Hauteur de la couche

Par défaut, la hauteur de couche est à 0,2mm. C'est un bon compromis entre vitesse et qualité d'impression.

**Réglages d'impression :**
![](images/images_doc_03/H.png)

Pour un meilleur rendu des détails, sélectionnez une épaisseur de couche plus fine.
Pour une impression plus rapide, par exemple pour les très grosses pièces, sélectionnez 0,3mm.
Pour en savoir plus sur la hauteur des canapés : [voir le site Prusa](https://help.prusa3d.com/fr/article/couches-et-perimetres_1748)

### c - Supports

Lorsqu'un élément est imprimé avec un angle de surplomb **inférieur à 45°**, il peut s'affaisser et nécessiter un support pour soutenir.

Par défaut, c'est aucun et il faudra donc choisir en fonction de votre pièce. Attention si vous ajoutez un support, le temps d'impression va se rallonger. Comme vous pouvez voir, il y'a 4 possibilitées et vous pouvez choisir comme cela vous convient.

- **Aucun (par défaut) :** à utiliser uniquement pour des pièces sans surplomb et avec de ponts de petite taille
- **Supports sur le plateau uniquement :** permet d'éviter d'avoir des supports qui commencent dans la pièce (ils sont plus difficiles à retirer)
- **Uniquement pour les générateurs de supports :** avec cette option, vous pouvez choisir où mettre des supports en ajoutant une forme à l'endroit voulu
- **Partout :** ajoute des supports partout où c'est nécessaire

Pour un résultat plus net, il est parfois préférable de jouer sur la distance de contact, l'espacement du motif d'interface, le nombre de canapés d'interface. Pour plus de details sur les supports cliquez [ici](https://help.prusa3d.com/fr/article/supports_1698)

![](images/images_doc_03/supports.png)

Pour mon impression, j'ai utilié **Partout** à cause de la forme de la pièce.
Voici le résultat avec ajout de support.

![](images/images_doc_03/Avec%20support.png) 

### d - Remplissage

Le remplissage de la densité est par défaut à 15 %. C'est un bon compromis entre rapidité et la solidité de la pièce. Sélectionnez un remplissage entre 0 % et 35 %. Au-dessus de 35%, vous ne gagnez plus en force. ça serait une perte de temps et un gaspillage de matériel.

### e - Exportation du code G

Cliquez sur le bouton Couper maintenant en bas à droite. Vous pouvez alors voir une estimation du temps d’impression. Si le temps d'impression est trop long, vous pouvez toujours modifier les paramètres tels que la hauteur du calque ou le remplissage, par exemple. Il a fallu 59 minutes pour imprimer mon dessin.
Si tout va bien, cliquez sur le bouton Exporter le G-code . Enregistrez le fichier G-code sur une carte SD dans un dossier à votre nom.

### 3.3 - Impression

Pour l'impression, il y'a quelques étapes à suivre. Voici les différentes étapes :

- Vérifiez que le filament installé correspond à celui indiqué dans le slicer

- Nettoyer le plateau

- Insérez la carte SD dans le panneau de contrôle, une liste avec les fichiers présents sur la carte doit s'afficher à l'écran

- Utilisez le bouton rond pour sélectionner votre fichier G-code dans la liste

Attendez que le préchauffage et le calibrage soient terminés pour voir si la première couche adhère bien au plateau.

  
Voici quelques photos que j'ai prise lors de l'impression des pieds et j'ai malhereusement oublié de prendre pour le corps et les bras. 

**Au début :**

![](images/images_doc_03/imp1.jpg)

**En cours d'impression :**

![](images/images_doc_03/imp2.jpg)



**Résultat final**

![](images/images_doc_03/toutes_les_peces.jpg)



![](images/images_doc_03/assemblées.jpg)

### Angry android en action

J'ai incorporé une brève vidéo dans laquelle mon Android illustre sa flexibilité en jetant les différentes pièces que j'ai imprimées, mais qui n'ont pas fonctionné comme prévu. Cliquez [ici](https://youtube.com/shorts/SmrJKp6x6Q0) pour voir.

# problème :

Lors de la première impression du corps, j'ai rencontré un problème de profondeur pour l'emboîtement des bras, le trou n'était pas assez profond. Étant donné que mon code était paramétré, j'ai résolu cette difficulté en ajustant simplement la profondeur, en augmentant la valeur de la hauteur de la profondeur, puis j'ai réimprimé la pièce.

![](images/images_doc_03/problème.jpg)









