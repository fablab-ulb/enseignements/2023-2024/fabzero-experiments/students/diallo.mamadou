# 2. Conception Assistée par Ordinateur (CAO)

Dans cette phase ultérieure du module, l'objectif consiste à acquérir des compétences dans l'utilisation de divers logiciels de CAO (conception assistée par ordinateur). Ces outils nous permettront de concevoir des modèles 3D variés, en vue de les assembler ultérieurement et de les imprimer à l'aide d'une imprimante 3D. Des informations détaillées sur l'impression 3D seront fournies dans le module suivant.

Les logiciels nécessaires ont été installés, et pour les télécharger, il vous suffit de cliquer dessus.

- [Inkscape](https://inkscape.org/fr/)
- [OpenSCAD](https://openscad.org/)
- [FreeCAd](https://www.freecad.org/)

## 2.1 Choix du logiciel

Suite à l'installation des logiciels que j'ai préalablement présentés à la demande du professeur, nous avons bénéficié d'une brève introduction et de quelques astuces essentielles concernant chacun d'entre eux au cours de la séance. Mon choix s'est porté sur OpenSCAD pour la conception des divers modèles que je devrai élaborer tout au long du cours.
Voici un petit aperçu de l'interface d'accueille de l'OpenSACD.

![](images/images_doc_02/OpenSCAD.png)

## 2.2 Projet 3D

Notre objectif était de constituer des groupes et de réfléchir à la conception d'un objet emboîtable et flexible, permettant à chaque étudiant d'assembler et d'imprimer une pièce. Ces pièces seraient ensuite rassemblées pour former un ensemble cohérent. Malheureusement, je n'ai pas pu intégrer un groupe car j'étais absent lors de la formation des équipes. En conséquence, je suis contraint de travailler en solo.

Après une période de réflexion approfondie, j'ai finalement eu l'idée de créer une pièce qui s'inspire du logo d'Android. Voici un aperçu de cette idée.

![](images/images_doc_02/idéé.jpg)

## 2.3 OpenSCAD 

Un aperçu de ce que sa ressemble sur openSCAD et pour accéder au fichier .scad, cliquez [ici](files/android-backup.scad)

![](images/images_doc_02/Corps.png)

Un aperçu de ce que sa ressemble sur openSCAD et pour accéder au fichier .scad, cliquez [ici](files/Pieds.scad)

![](images/images_doc_02/Pieds.png)

Un aperçu de ce que sa ressemble sur openSCAD et pour accéder au fichier .scad, cliquez [ici](files/Le_Bras.scad)

![](images/images_doc_02/Les_bras.png)



## 2.4 Licences CC (Creative Commons)



Les licences Creative Commons sont un ensemble de licences de droits d'auteur qui permettent aux créateurs de partager leur travail avec d'autres tout en définissant les conditions sous lesquelles leur travail peut être utilisé, partagé et remixé. Elles visent à encourager la diffusion de la culture et de la créativité tout en permettant aux créateurs de conserver certains droits sur leur travail.

Il existe plusieurs types de licences Creative Commons, chacune offrant différentes permissions. Voici les principales licences Creative Commons :

**- 1 Attribution (CC BY):** Cette licence permet à quiconque d'utiliser, de modifier, de distribuer et de créer des œuvres dérivées à condition de donner le crédit à l'auteur original.

**- 2 Attribution-ShareAlike (CC BY-SA):** Cette licence est similaire à CC BY, mais elle exige également que les œuvres dérivées soient diffusées sous la même licence.

**- 3 Attribution-NoDerivs (CC BY-ND):** Cette licence permet l'utilisation de l'œuvre à condition qu'aucune œuvre dérivée ne soit créée, et que le crédit soit donné à l'auteur original.

**- 4 Attribution-NonCommercial (CC BY-NC):** Cette licence permet d'utiliser, de modifier et de distribuer l'œuvre à des fins non commerciales uniquement, avec attribution à l'auteur.

**- 5 Attribution-NonCommercial-ShareAlike (CC BY-NC-SA):** Similaire à CC BY-NC, cette licence exige également que les œuvres dérivées soient diffusées sous la même licence et qu'elles ne soient utilisées qu'à des fins non commerciales.

**- 6 Attribution-NonCommercial-NoDerivs (CC BY-NC-ND):** Cette licence est la plus restrictive, permettant uniquement le téléchargement de l'œuvre et le partage avec d'autres, à condition qu'aucune modification ne soit apportée, qu'aucune utilisation commerciale ne soit autorisée et que le crédit soit donné à l'auteur.

Pour avoir plus de détails sur les licences, vous pouvez tout simplement cliquez [ici](https://creativecommons.org/share-your-work/cclicenses/)


Personnellement, j'ai pris le [CC BY-SA 4.0] qui est la deuxième de la liste que j'ai  énumèré en haut.


je l'ai choisie pour favoriser la collaboration et la libre circulation des idées. Elle permet aux créateurs de construire sur le travail d'autrui en exigeant que les œuvres dérivées soient partagées sous la même licence. Cela garantit l'ouverture, protège la liberté des utilisateurs et encourage un écosystème créatif dynamique où les idées circulent librement. La simplicité d'utilisation de la licence Creative Commons en fait un choix accessible pour ceux qui souhaitent promouvoir la collaboration et la diffusion de la connaissance.




