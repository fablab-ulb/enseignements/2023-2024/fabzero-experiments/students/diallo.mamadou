# 5. Dynamique de groupe et projet final

Ce module comporte  3 partie différentes qui vont résumées ce qu'on a fait durant trois semaine et ces différentes parties sont:

- a - Analyse et conception de projet
- b - Constitution de groupes et réflexion sur des enjeux sociétaux 
- c - La dynamique de groupe 

## 5.1 - Analyse et conception de projet
### a - L'arbres à problemes:

L'expression « arbre à problèmes » ou « arbre des problèmes » est souvent utilisée dans le cadre de la résolution de problèmes et de la prise de décision. C'est un outil visuel et analytique utilisé dans divers domaines tels que la gestion de projet, l'ingénierie, la résolution de problèmes.
Elle vise à représenter graphiquement les causes et les effets d'un problème donné.
Pour avoir une petitte introduction, vous pouvez cliquer [ici](https://www.youtube.com/watch?app=desktop&v=9KIlK61RInY)

### Structure :

  - **Tromc** : Identifie le **problème** principal <br>
  - **Racines** :  identifier les principales **causes** du problème (**Racines**) et les causes derrière les causes (**racines plus profondes**)<br>
  - **Les branches** : identifier le problème, **conséquences** directes (**branches**) et effets secondaires (**ramifications**)<br>

voici un aperçu de l'arbre.

![](images/images_doc_05/arbre1-2.jpg)

### b - L'arbres à objetifs ou solutions:

L'arbre à objectifs est souvent utilisé en complément de l'arbre à problèmes dans le cadre de processus de résolution de problèmes et de prises de décision. Alors que l'arbre à problèmes vise à analyser les causes d'un problème, l'arbre à objectifs est utilisé pour identifier et hiérarchiser les objectifs ou les solutions possibles associés à chaque branche de l'arbre à problèmes.
En résumé, l'arbre à objectifs aide à élaborer des solutions ou des objectifs concrets pour résoudre les problèmes identifiés à l'aide de l'arbre à problèmes, et cela permet de structurer et de prioriser les actions à entreprendre pour atteindre ces objectifs.

### Structure :

  ▪ **Tronc** : le **problème** est reformulé en **Objectif**<br>
  ▪ **Racines** : les **causes** sont reformulées par **activités** à développer pour atteindre l'objectif<br>
  ▪ **Branches** : les **consequnces** du problème sont reformulées **Résultats** du projet<br>

voici un aperçu de l'arbre.

![](images/images_doc_05/arbre_des_objectifs.png)

### c - Affectation :

Dans cette partie, on doit individuellement ou en binôme de support, nous inspirez [des projets de fablab](https://class-website-fablab-ulb-enseignements-2023-2024-c4a40531a3337f.gitlab.io/#archives) et [des projets scientifiques frugaux](https://gitlab.com/fablab-ulb/enseignements/fabzero/projects/-/blob/main/open-source-lab.md) présentés en cours et choisir un projet qui vous tient à cœur. En suite, ont doit analyser le projet et construire un arbre à problèmes et un arbre à objectifs. 

J'ai choisi les maladies comme problème central et j'ai élaboré un arbre à problèmes et objectifs axé sur cette thématique.

**Arbre à problème :**

![](images/images_doc_05/Amaladies.jpg)

**Arbre à objectifs :**

![](images/images_doc_05/Asolutions.jpg)


Pour les descriptions, regardez les arbres présentés au-dessus. 


## 5.2 - Constitution de groupes et réflexion sur des enjeux sociétaux


La tenue de cette séance s'est révélée d'une importance capitale, car elle a marqué la constitution des groupes définitifs pour notre projet final.
Nous étions invités à apporter un objet symbolisant une problématique qui nous touche particulièrement. J'ai consacré du temps avant la réunion à réfléchir aux diverses problématiques qui me préoccupent, et au final, j'avais prévu d'apporter des médications ce qui devrait symboliser les maladies. 

Dans le cours suivant, le professeur m'a autorisé à circuler entre les différents groupes pour observer le sujet de chaque groupe afin que je puisse faire un choix. Heureusement, j'ai trouvé un groupe qui travaillait sur le thème des maladies, j'ai donc décidé de rejoindre ce groupe.

### Pourquoi les maladies ?

Je suis originaire d'un pays sous-développé et depuis toujours, la question de la santé représente un défi majeur qui me tient particulièrement à cœur. Les enjeux de santé comprennent une multitude de problèmes tels que les maladies infectieuses comme le paludisme, la tuberculose et le VIH/SIDA, les lacunes en nutrition, les maladies liées à une eau non potable, les complications en matière de santé maternelle et infantile, ainsi que les maladies non transmissibles telles que les maladies cardiovasculaires et le diabète. Ces défis de santé sont amplifiés par des conditions socio-économiques précaires et une accessibilité restreinte aux services de santé.



## 5.3 - La dynamique de groupe

Dans cette unité, nous avons acquis les compétences nécessaires pour optimiser notre travail en groupe. Nous avons également appris à organiser efficacement nos réunions, à définir et à assigner des rôles, à appliquer des méthodes de prise de décision, à communiquer de manière collaborative, à encourager la participation de chacun et à offrir des retours constructifs.
Voici [les Slides de la présentation Dynamique de Groupe](https://class-website-fablab-ulb-enseignements-2023-2024-c4a40531a3337f.gitlab.io/files/Fabzero-Exp-Group-Dynamics-presentation.pdf) qu'on a mis à notre disposition. Vous pouvez aussi jeter un coup d'oille. 

Durant la cours, plusieurs outils nous avons été présentés, j'ai décidé d'en présenter trois d'entre eux :

### a - Le déroulement de la réunion

À mon sens, c'est l'un des éléments les plus cruciaux au sein d'un projet. Il garantit que les réunions et, plus largement, le projet se déroulent de manière claire, rapide et surtout efficace. 

L'établissement du plan de la réunion peut être effectué soit en amont, soit en tout début de celle-ci. Préparer le plan avant la réunion me semble être la méthode la plus efficiente, car cela permet à tous les membres du groupe d'avoir connaissance des points à aborder et d'éventuellement proposer des ajouts avant le début de la réunion.

### b - La répartition des différents rôles

La répartition des rôles au sein d'un groupe est essentielle pour le bon déroulement des réunions. Il est primordial d'opter pour une rotation équitable des responsabilités parmi les membres, évitant ainsi que les mêmes personnes endossent systématiquement les mêmes tâches.

- Secrétaire : Le rôle du secrétaire ou de la secrétaire revêt une importance capitale. Cette personne est chargée de consigner méticuleusement les points abordés, les décisions prises et les actions à entreprendre lors de la réunion. Les traces écrites permettent d'assurer le suivi des discussions et d'établir un compte rendu précis pour référence future.

- Animateur : l'animateur ou l'animatrice, sa fonction est d'animer les échanges et de maintenir la dynamique de la réunion. En identifiant les moments où les discussions stagnent ou s'éloignent du sujet principal, l'animateur relance les débats pour favoriser des échanges constructifs et une prise de décision efficace.

- Time keeper : le gestionnaire du temps joue un rôle crucial en étant le gardien du chronomètre. Il s'assure que la réunion reste dans les limites temporelles fixées, ce qui permet un partage équilibré de la parole entre les participants. En régulant les interventions pour éviter les digressions et en assignant des plages horaires à chaque sujet à traiter, il garantit une gestion efficiente du temps de réunion.

### c - La météo d'entrée

Cette pratique consiste en un moment où chaque membre du groupe partage son état d'esprit. Je considère qu'il est fondamental que chacun se sente à l'aise et connecté aux autres participants. Parfois, diverses circonstances font qu'une personne n'est pas pleinement disponible ou réceptive le jour de la réunion. En instaurant un système simple de signalisation, cela permet de désamorcer toute potentielle tension ou incompréhension au sein du groupe. Cette étape peut sembler anodine, ne nécessitant que quelques minutes, cependant, elle contribue grandement à maintenir une atmosphère de collaboration et de soutien mutuel au sein de l'équipe. Le simple fait de donner à chacun la possibilité d'exprimer brièvement son ressenti peut créer un espace plus ouvert et bienveillant pour nos échanges et nos décisions collectives.


































